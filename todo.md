Réalisation d'une PWA sur le thème d'une application livraison de repas.

Cette application devra reprend l’ensemble des concept vue en cours afin de fournir la meilleure Expérience Utilisateur possible

---

# Open Eat

## Fonctionnalité :

- Firebase (Cloud function / realtime database / Cloud Firestore)
- UI qui montre l'état de la connexion (online / Offline)
  v Authentification
  v Passer une commande
  v Rechercher un établissement
  v Favoris
  v Notation (note sur 5)
  v Commentaires
  v Avatar utilisateur
  v Image (lazy loading)
  v View lazy loading
  v Synchronisation (Online / offline)
- Web push notification

## Notation

- Projet
  - PWA lighthouse score (ALL)
  - Fonctionnalité bonus
    - Direct message
  - Code
  - Good UX
- Questions
