import React from "react";

import useCompanies from "../../hooks/useCompanies";
import useUser from "../../hooks/useUser";

import CompanyCard from "../CompanyCard";
import BackHeader from "../BackHeader";

const Favorites = () => {
  const { selectors } = useCompanies();
  const { selectors: userSelectors } = useUser();

  const favorites = userSelectors.getFavorites();

  return (
    <>
      <BackHeader title="Mes favoris" />
      <div className="page-content page-favorites">
        <section>
          {favorites
            ? favorites.map((isFavorite) => {
                if (!isFavorite[1]) return <></>;

                let company = selectors.getCompany(isFavorite[0]);
                return <CompanyCard key={company.id} company={company} />;
              })
            : "Pas de favoris"}
        </section>
      </div>
    </>
  );
};

export default Favorites;
