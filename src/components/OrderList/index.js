import React from "react";
import { NavLink, useParams } from "react-router-dom";

import useUser from "../../hooks/useUser";

import OrderCard from "../OrderCard";

const OrderList = () => {
  const { selectors } = useUser();
  let { type } = useParams();

  var orders = [];

  if (type === "incoming") {
    orders = selectors.getIncomingOrders();
  } else {
    orders = selectors.getOrders();
  }

  //Effectue l'action fetchOrders() de useUser qui permet d'alimenter la propriété orders avec les commandes

  return (
    <>
      <header className="topbar">
        <NavLink to="/orders" exact className="btn tab flex-grow">
          Commandes <br /> validées
        </NavLink>
        <NavLink to="/orders/incoming" exact className="btn tab flex-grow">
          Commandes <br />
          en attentes
        </NavLink>
      </header>
      <div className="page-content page-orders">
        <section className="orders content-block">
          {Object.values(orders).length > 0 ? (
            Object.values(orders).map((order, key) => (
              <OrderCard order={order} key={key} />
            ))
          ) : (
            <div className="text-center">
              Vous n'avez pas de commandes{" "}
              {type === "incoming" ? "en attente" : "passées"}
            </div>
          )}
        </section>
      </div>
    </>
  );
};

export default OrderList;
