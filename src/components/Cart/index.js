import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";

import useUser from "../../hooks/useUser";
import useCompanies from "../../hooks/useCompanies";
import useUI from "../../hooks/useUI";
import useAuth from "../../hooks/useAuth";

import BackHeader from "../BackHeader";

import moment from "moment";

const Cart = () => {
  const history = useHistory();

  const { selectors: userSelectors, actions: userActions } = useUser();
  const cart = userSelectors.getCart();
  const cartItems = userSelectors.getCartItems();

  const { user } = useAuth();

  const { selectors: companiesSelectors } = useCompanies();
  const company = companiesSelectors.getCompany(cart.company);

  const { selectors: UISelectors } = useUI();

  const [pranked, setPranked] = useState(0);

  function createOrder() {
    const order = {
      timestamp: moment().unix(),
      date: moment().format(),
      ...cart,
      uid: user.uid,
      total_price: cart.total_price + 3,
    };

    if (UISelectors.isOnline()) {
      order.statut = "waiting";
      userActions.syncOrder(order);
    } else {
      order.statut = "local";
      userActions.createOrder(order);
    }

    /* Vider le panier */
    userActions.setCart([]);

    /* Redirction */
    history.push("/orders");
  }

  function emptyCart() {
    userActions.setCart([]);
  }

  if (!cart.items || cart.items.length <= 0) {
    return (
      <>
        <BackHeader title="Mon panier" />
        <div className="page-content content-block text-center">
          Votre panier est vide.
        </div>
      </>
    );
  }

  return (
    <>
      <BackHeader title="Mon panier" />
      <div className="page-content page-cart">
        <section className="content-block text-center">
          <h2>{company.name}</h2>
        </section>

        <section className="content-block items-recap more-hpadding">
          <div className="flex flex-justify-between">
            <h3>Votre commande</h3>
            <button className="link" onClick={emptyCart}>
              Vider le panier
            </button>
          </div>

          <div className="items">
            {cartItems &&
              cartItems.map((item, index) => {
                const service = companiesSelectors.getService(
                  company,
                  item.service
                );

                return (
                  <div className="item" key={index}>
                    <div className="name">
                      {item.quantity} x {service.name}
                      {item.options &&
                        Object.entries(item.options).map((option) => (
                          <div className="option" key={option[0]}>
                            {option[0]}: {option[1].name}
                          </div>
                        ))}
                    </div>
                    <div className="price">{item.total_price}</div>
                  </div>
                );
              })}
          </div>
        </section>

        <section className="promo-code">
          {pranked ? (
            <div>Pranked, pas de promotions chez nous !</div>
          ) : (
            <div className="flex flex-justify-between total">
              <div>Un code promo ?</div>
              <button className="link" onClick={() => setPranked(1)}>
                Ajoutez le!
              </button>
            </div>
          )}
        </section>

        <section className="content-block price-recap more-hpadding">
          <div className="flex flex-justify-between total">
            <div className="title">Sous-total</div>
            <div className="price">{cart.total_price}</div>
          </div>
          <div className="flex flex-justify-between additional-price">
            <div className="title small">Déplacement</div>
            <div className="price">3</div>
          </div>
          <div className="flex flex-justify-between final-price">
            <div className="title">Total</div>
            <div className="price">{cart.total_price + 3.0}</div>
          </div>
        </section>

        <div className="content-block">
          {user ? (
            <button
              type="button"
              className="btn btn-valid"
              onClick={createOrder}
            >
              Passer la commande {UISelectors.isOnline() ? "" : "(Hors ligne)"}
            </button>
          ) : (
            <Link to="/login" className="btn btn-valid btn-flex">
              Se connecter pour commander
            </Link>
          )}
        </div>
      </div>
    </>
  );
};

export default Cart;
