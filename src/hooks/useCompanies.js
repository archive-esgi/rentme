import { useContext } from "react";
import MainContext from "../contexts/mainContext";
import { fetchServices as aFetchServices } from "../contexts/actions/services";
import {
  addVote as aAddVote,
  fetchVotes as aFetchVotes,
} from "../contexts/actions/companies";

const useCompanies = () => {
  const {
    state: { companies: companiesState },
    dispatch,
  } = useContext(MainContext);

  const actions = {
    fetchServices: (company) =>
      !companiesState.services[company.id] &&
      dispatch({
        type: "RECEIVE_SERVICES",
        payload: {
          company,
          data: aFetchServices(company),
        },
      }),
    addVote: (vote) => {
      aAddVote(vote).then(() => {});
    },
    fetchVotes: (company) => {
      company.id &&
        !companiesState.votes[company.id] &&
        aFetchVotes(company).then((data) =>
          dispatch({
            type: "RECEIVE_VOTES",
            payload: {
              company,
              data: data,
            },
          })
        );
    },
  };

  const selectors = {
    getService: (company, serviceId) =>
      companiesState.services[company.id]
        ? companiesState.services[company.id][serviceId] || []
        : [],
    getServices: (company) => companiesState.services[company.id] || [],
    getVotes: (company) => companiesState.votes[company.id] || [],
    getCompanies: () => companiesState.companies,
    getCompany: (id) => companiesState.companies[id] || [],
    searchCompanies: (name) =>
      companiesState.companies.filter((company) =>
        company.name.toLowerCase().includes(name.toLowerCase())
      ),
    getScore: (company) => {
      const votes = selectors.getVotes(company);
      return votes.length
        ? (
            parseFloat(
              votes.reduce((acc, val) => acc + parseInt(val.score), 0)
            ) / votes.length
          ).toFixed(2)
        : 0;
    },
  };
  return { selectors, actions };
};

export default useCompanies;
