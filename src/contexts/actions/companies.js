import "firebase/database";
import "firebase/firestore";
import firebase from "firebase/app";

export const addVote = (vote) =>
  firebase
    .firestore()
    .collection("vote")
    .add(vote)
    .then((docRef) => {
      return { ...vote, id: docRef.id };
    });

export const fetchVotes = (company) =>
  firebase
    .firestore()
    .collection("vote")
    .orderBy("date", "desc")
    .where("company", "==", company.id)
    .get()
    .then((snapshot) =>
      snapshot.docs.map((doc) => {
        return { ...doc.data(), id: doc.id };
      })
    );

export const fetchFavorites = () =>
  Promise.resolve().then(() => JSON.parse(localStorage.getItem("favorites")));

export const setFavorites = (favorites) =>
  localStorage.setItem("favorites", JSON.stringify(favorites));

export const fetchCompanies = () => [
  {
    id: 0,
    name: "Beauty Paris",
    categories: ["Bien-être", "Nature"],
    desc:
      "Salon de bien-être a domicile grâce à nos services chez vous! Passez sous les mains de nos experts. Un moment de détente garanti!",
    image: "massage",
  },
  {
    id: 1,
    name: "Together pump",
    categories: ["Sport", "Musculation"],
    desc:
      "Je suis Julien, un coach sportif professionnel depuis 5 ans maintenant. Je vous accompagne dans toutes vos séances.",
    image: "sport_coach",
  },
  {
    id: 2,
    name: "Les buchebros",
    categories: ["Entretient", "Jardin"],
    desc:
      "Deux frêres qui proposent leurs services de bucheron et d'entretient vert de vos jardins.",
    image: "bucheron",
  },
  {
    id: 3,
    name: "Nounou Top",
    categories: ["Garde d'enfants"],
    desc:
      "Une équipe de tendres nourices pour garder vos enfants, laissez vos enfants entre de bonnes mains !",
    image: "nourice",
  },
  {
    id: 4,
    name: "Les friandises de Clara",
    categories: ["Cuisine", "Friandises"],
    desc:
      "Je confectionne des gateaux et friandises pour des évènements, fêtes ou anniversaires.",
    image: "gateaux",
  },
];
