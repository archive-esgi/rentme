export const initialState = {
  online: true,
};

/**
 * action = { type: String, payload: any }
 */
export const reducer = (state, action) => {
  switch (action.type) {
    case "RECEIVE_ONLINE":
      return {
        ...state,
        online: action.payload.data,
      };
    default:
      return state;
  }
};
